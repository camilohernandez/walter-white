/**
 * File navigation.js.
 *
 * Handles toggling the navigation menu for small screens and enables TAB key
 * navigation support for dropdown menus.
 */

// Javascript funcitons
jQuery(document).ready(function () {
	    

        // NAV BURGUER
       	jQuery( ".menu-toggle" ).click(function() {
			event.stopPropagation();
         	
         	jQuery(".nav-home-menu").slideToggle(250);	

         	/*
			if(jQuery(this).hasClass('open-nav-button')) {
				jQuery(this).removeClass( "open-nav-button" );
				jQuery(this).addClass( "close-nav-button" );
			}else {
				jQuery(this).removeClass( "close-nav-button" );
				jQuery(this).addClass( "open-nav-button" );
			}
			*/
			
       	});
	   	jQuery('menu-toggle').on('click touchend', function(e) {
			var el = $(this);
			var link = el.attr('href');
			window.location = link;
	   	});


	   	if (jQuery('#back-to-top').length) {
		    var scrollTrigger = 220, // px
		        backToTop = function () {
		            var scrollTop = jQuery(window).scrollTop();
		            if (scrollTop > scrollTrigger) {
		                jQuery('#back-to-top').fadeIn("slow");
		            } else {
		                jQuery('#back-to-top').fadeOut("fast");
		            }
		        };
		    backToTop();
		    
		    jQuery(window).on('scroll', function () {
		        backToTop();
		    });
		    
		    jQuery('#back-to-top').on('click', function (e) {
		        e.preventDefault();
		        jQuery('html,body').animate({
		            scrollTop: 0
		        }, 700);
		    });
		}


	   	

		   	
     
});
