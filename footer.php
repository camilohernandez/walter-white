<?php
/**
 * @package walter_white
 */
?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		
		<div class="menu-footer">
		    <!-- Menu displayed -->
		    <?php
			// Make sure there is a social menu to display.
			if ( has_nav_menu( 'social' ) ) { ?>
			<nav class="social-menu">
				<?php
					wp_nav_menu( array(
						'theme_location' => 'social',
						'menu_class'     => 'social-links-menu',
						'depth'          => 1,
						'link_before'    => '<span class="screen-reader-text">',
						'link_after'     => '</span>' . walter_get_svg( array( 'icon' => 'chain' ) ),
					) );
				?>
			</nav><!-- .social-menu -->
			<?php } ?>

		</div>



		<div class="site-info">
			<p>Copyright @ 2017 <a href="http://www.camiloncho.com">Cam</a></p>
			<p>Walter White theme (Version 1.0.0)</p>
			<p>Proudly powered by <a href="https://wordpress.com/" target="_blank">WordPress</a></p>		
		</div><!-- .site-info -->

	</footer><!-- #colophon -->
	<a href="#!" id="back-to-top">
		<div class="back-to-top">
			<?php echo walter_get_svg( array( 'icon' => 'top', 'fallback' => true ), 'walter_white'); ?>
		</div>
	</a>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
