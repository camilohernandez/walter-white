<?php
/**
 * @package walter_white
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<div class="content-article-inner"> <!-- Forn the inner wrapper -->
				<section class="error-404 not-found">
					
					<header class="page-header">
						<h1 class="page-title"><?php esc_html_e( 'Oops! That page cannot be found.', 'walter_white' ); ?></h1>
					</header><!-- .page-header -->

					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
						<img src="<?php echo get_template_directory_uri() ?>/images/404.svg" alt="">
					</a>

				</section><!-- .error-404 -->
			</div>
			

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
