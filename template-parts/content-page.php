<?php
/**
 * @package walter_white
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="content-article-image"> <!-- Forn the thumb image wrapper -->
		<?php
			the_post_thumbnail('walter_white_thumb_full');
		?>
	</div>

	<div class="content-article-inner"> <!-- Forn the inner wrapper -->
		
		<header class="entry-header">
			<?php
			if ( is_single() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
			else :
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			endif;

			if ( 'post' === get_post_type() ) : ?>
			<div class="entry-meta">
				<?php walter_white_posted_on(); ?>
			</div><!-- .entry-meta -->
			
			<?php walter_white_entry_footer(); ?> <!-- .entry-footer -->

			<?php
			endif; ?>
		</header><!-- .entry-header -->

		<div class="entry-content">
			<?php

				if( is_front_page() || is_home() || is_archive() ){
					the_excerpt();
				}else{
					
					the_content( sprintf(
						//translators: %s: Name of current post. 
						wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'walter_white' ), array( 'span' => array( 'class' => array() ) ) ),
						the_title( '<span class="screen-reader-text">"', '"</span>', false )
					) );

					wp_link_pages( array(
						'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'walter_white' ),
						'after'  => '</div>',
					) );
					
				}
			?>
		</div><!-- .entry-content -->

		<footer class="entry-footer">
			<?php //walter_white_entry_footer(); ?>
		</footer><!-- .entry-footer -->

	</div>

</article><!-- #post-## -->
