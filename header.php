<?php
/**
 * @package walter_white
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<!-- Keyboard access -->
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'polar' ); ?></a>
	
	<!-- .header-image -->
	<?php if ( get_header_image() && is_front_page() ) : ?>
	<figure class="header-image">
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
			<img src="<?php header_image(); ?>" width="<?php echo esc_attr( get_custom_header()->width ); ?>" height="<?php echo esc_attr( get_custom_header()->height ); ?>" alt="">
		</a>
	</figure>
	<?php endif; // End header image check. ?>


	<header id="masthead" class="site-header" role="banner">

		<div class="site-branding">
			
			<?php the_custom_logo(); ?> <!-- site-custom-logo -->

			<div class="site-branding-text">
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<?php
				$description = get_bloginfo( 'description', 'display' );
				if ( $description || is_customize_preview() ) : ?>
					 <p class="site-description"><?php //echo $description; //Remove Description from theme w_w /* WPCS: xss ok. */ ?></p>
				<?php
				endif; ?>
			</div><!-- .site-branding__text -->

		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation" role="navigation">
			<button class="menu-toggle open-nav-button">
				<?php echo walter_get_svg( array( 'icon' => 'nav', 'fallback' => true ), 'walter_white'); ?>
			</button>		
		</nav><!-- #site-navigation -->

		<!-- Menu displayed -->
		<div class="nav-home-menu">
			
			<?php wp_nav_menu( array( 'theme_location' => 'menu-1', 'menu_id' => 'primary-menu' ) ); ?>	

			<div class="widget-header">
				<?php dynamic_sidebar( 'header-sidebar' ); ?>
			</div>

			
		</div>
		

	</header><!-- #masthead -->

	<div id="content" class="site-content">
