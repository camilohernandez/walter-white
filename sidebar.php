<?php
/**
 * @package walter_white
 */

if (  !is_active_sidebar( 'footer-left-sidebar' ) && !is_active_sidebar( 'footer-right-sidebar' )   ) {
	return;
}
?>

<aside id="secondary" class="widget-area" role="complementary">
	<div class="widget-left">
		<?php dynamic_sidebar( 'footer-left-sidebar' ); ?>
	</div>
	<div class="widget-right">
		<?php dynamic_sidebar( 'footer-right-sidebar' ); ?>
	</div>
</aside><!-- #secondary -->
