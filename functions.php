<?php
/**
 * @package walter_white
 */


if ( ! function_exists( 'walter_white_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function walter_white_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on w-w, use a find and replace
	 * to change 'w-w-paul' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'walter-white', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Header Images
	 */
	add_theme_support( 'custom-header', apply_filters( 'walter_white_custom_header_args', array(
		'default-image'          => '',
		'default-text-color'     => '000000',
		'width'                  => 2000,
		'height'                 => 350,
		'flex-height'            => true,
	) ) );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 */
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'walter_white_thumb_full', 1000, 300, true );



	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'menu-1' => esc_html__( 'Primary', 'walter-white' ),
		'social' => esc_html__( 'Social', 'walter-white' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'walter_white_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for Custom Logo
	add_theme_support( 'custom-logo', array(
		'width' => 60,
		'height' => 60,
		'flex-width' => true,
	));

	/* Editor styles */
	add_editor_style( array( 'inc/editor-styles.css', walterWhite_fonts_url() ) );

}
endif;
add_action( 'after_setup_theme', 'walter_white_setup' );


/**
 * Register custom fonts ::
 */
function walterWhite_fonts_url() {
	$fonts_url = '';
	/**
	 * Translators: If there are characters in your language that are not
	 * supported by Asap.
	 */
	$Asap = _x( 'on', 'Asap font: on or off', 'walter-white' );

	$font_families = array();
	
	if ( 'off' !== $oswald ) {
		$font_families[] = 'Asap:400,400i,700';
	}
	
	
	if ( in_array( 'on', array($Tinos, $Asap) ) ) {

		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
			'subset' => urlencode( 'latin,latin-ext' ),
		);

		$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
	}

	return esc_url_raw( $fonts_url );
}






/**
 * Register widget area footer left.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function walter_white_widgets_init() {
	
	register_sidebar( array(
		'name'          => esc_html__( 'Footer left', 'walter-white' ),
		'id'            => 'footer-left-sidebar',
		'description'   => esc_html__( 'Add widgets here.', 'walter-white' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Right', 'walter-white' ),
		'id'            => 'footer-right-sidebar',
		'description'   => esc_html__( 'Add widgets here.', 'walter-white' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Header', 'walter-white' ),
		'id'            => 'header-sidebar',
		'description'   => esc_html__( 'Add widgets here.', 'walter-white' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'walter_white_widgets_init' );



/**
 * Enqueue scripts and styles.
 */
function walter_white_scripts() {
	
	/* 	Enqueue Google Fonts ---------- */
	wp_enqueue_style( 'walter-white-style-googlefonts', walterWhite_fonts_url() );
	
	/* 	Enqueue Main Style ---------- */
	wp_enqueue_style( 'walter-white-style', get_stylesheet_uri() );

	/* 	Enqueue Nav Script ---------- */
	wp_enqueue_script( 'walter-white-navigation', get_template_directory_uri() . '/js/navigation.js', array(jquery), true );

	/* 	Enqueue Function Script ---------- */
	wp_enqueue_script( 'walter-white-functions', get_template_directory_uri() . '/js/functions.js', array('jquery'), true );

	/* 	Enqueue Skip-Link ---------- */
	wp_enqueue_script( 'walter-white-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), true );

}
add_action( 'wp_enqueue_scripts', 'walter_white_scripts' );



/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Load SVG icon functions.
 */
require get_template_directory() . '/inc/icon-functions.php';








